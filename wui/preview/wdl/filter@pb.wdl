<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/_edt/filters/filter.model"/>
	<sm:axis code="pb"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:content wedlet="BoxFreeDom">
					<sm:addAttribute name="lib">PREVIEW</sm:addAttribute>
					<sm:addAttribute name="method">filterPb</sm:addAttribute>
				</sm:content>
			</sm:openEdtWidget>
		</sm:root>
	</sm:editPoints>
</sm:dataFormBoxWdl>