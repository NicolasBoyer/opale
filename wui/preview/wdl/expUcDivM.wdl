<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Uc/expUcDivM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="title">
			<sm:skippedWidget>
				<sm:container style="font-size: 1.2rem;" appendSharedCssKey="custom-title"/>
			</sm:skippedWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>