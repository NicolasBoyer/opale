<?xml version="1.0"?>
<sma:assmntUnitEditBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:anyAssmntUnitEditPrim/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:questionMember>
			<sm:skippedWidget/>
		</sm:questionMember>
		<sm:interactionMember>
			<sm:editInteractionBoxWidget>
				<sm:stringFieldBox refCode="asw">
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:stringFieldBox>
				<sm:numberFieldBox refCode="asw">
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:numberFieldBox>
				<sm:gapTextBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:gapTextBox>
			</sm:editInteractionBoxWidget>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:skippedWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitEditBoxWdl>