<?xml version="1.0"?>
<sma:assmntUnitMlqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/match.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:questionMember>
			<sm:skippedWidget/>
		</sm:questionMember>
		<sm:interactionMember>
			<sm:mlqInteractionBoxWidget>
				<sm:groupBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:groupBox>
				<sm:groupLabelsBox focusable="true">
					<sm:container style="margin-left: 15px;"/>
				</sm:groupLabelsBox>
				<sm:trapsBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:trapsBox>
				<sm:targetBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:targetBox>
				<sm:labelBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:labelBox>
			</sm:mlqInteractionBoxWidget>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:skippedWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMlqBoxWdl>