<?xml version="1.0"?>
<sma:assmntUnitMcqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:anyAssmntUnitMcqPrim/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:questionMember>
			<sm:skippedWidget/>
		</sm:questionMember>
		<sm:choicesMember>
			<sm:choicesBoxWidget>
				<sm:choicesBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:choicesBox>
				<sm:choicesEntryBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:choicesEntryBox>
				<sm:choiceLabelBox>
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:choiceLabelBox>
				<sm:choiceExplanationBox orientation="horizontal">
					<sm:container>
						<sm:head style="display:none;"/>
					</sm:container>
				</sm:choiceExplanationBox>
				<sm:solutionBox>
					<sm:container>
						<sm:cssRules xml:space="preserve">:host &gt; .head &gt; .label {
display:none;
}</sm:cssRules>
					</sm:container>
				</sm:solutionBox>
				<sm:choiceSingleSolutionProp>
					<sm:label style="display:none;"/>
				</sm:choiceSingleSolutionProp>
			</sm:choicesBoxWidget>
		</sm:choicesMember>
		<sm:globalExplanationMember>
			<sm:skippedWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMcqBoxWdl>