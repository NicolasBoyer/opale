<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:anyCompositionPrim/>
	<sm:editPoints>
		<sm:tag refCodes="*">
			<sm:skippedWidget/>
		</sm:tag>
		<sm:tag refCodes="div">
			<sm:skippedWidget>
				<sm:container style="margin: 0.8rem 0 1rem 1rem;"/>
			</sm:skippedWidget>
		</sm:tag>
		<sm:tag refCodes="pb">
			<sm:skippedWidget>
				<sm:container style="margin: 0.5rem 0 0.5rem 1rem;"/>
			</sm:skippedWidget>
		</sm:tag>
		<sm:tag refCodes="obj intro conclu pre">
			<sm:openEdtWidget>
				<sm:headBodyContentBox collapsed="forbidden">
					<sm:cssRules xml:space="preserve">:host&gt;.head {
    font-size: 1.4rem;
    margin-bottom: 0.5rem;
    background-image: linear-gradient(to right, #434e52 0%,rgba(0,0,0,0) 100%);
    background-size: 100% 2px;
    background-position: bottom;
    background-repeat: no-repeat;
    padding-bottom: 4px;
}</sm:cssRules>
					<sm:head>
						<sm:extractFromModelCtx>
							<sm:name/>
						</sm:extractFromModelCtx>
					</sm:head>
					<sm:body>
						<sm:call>
							<sm:subModel/>
						</sm:call>
					</sm:body>
				</sm:headBodyContentBox>
			</sm:openEdtWidget>
		</sm:tag>
		<sm:tag refCodes="info def ex rem adv warning comp meth remind basic syntax legal simul">
			<sm:openEdtWidget>
				<sm:headBodyContentBox collapsed="forbidden">
					<sm:cssRules sc:refUri="/wui/preview/wdl/xTags.doss" xml:space="preserve">:host {
	background-image: linear-gradient(0deg, #434e52 0%, rgba(0, 0, 0, 0) 100%);
	background-position: right bottom;
	background-repeat: no-repeat;
	background-size: 1px 100%;
}

:host&gt;.body {
	background-color:rgba(224, 236, 236,0.5);
	border-radius:0 0 0 5px;
	background-image: linear-gradient(to right, #434e52 0%, rgba(0, 0, 0, 0) 100%);
	background-position: center top;
	background-repeat: no-repeat;
	background-size: 100% 1px;
	padding: 1px 4px 4px 4px;
	margin-bottom:15px;
	margin-right:1px;
}
:host&gt;.head {
	font-size: 1.2em;
	margin-bottom: 0px;
	line-height: 2.5em;
	color:#434e52;
	text-align:right;
}
:host&gt;.head i.type {
	background:url("[CSSLIB]/blocks.svg") no-repeat scroll transparent;
	flex: 0 0 auto;
    height: 2.5em;
    margin-right: 0.8rem;
    order: 2;
    padding-left: 2.5em;
font-weight: bold;
}
:host&gt;.head span.title {
    flex: 1 1 auto;
    margin-left: 0.8rem;
    order: 1;
    text-align: left;
}
:host([wed-name='info#'])&gt;.head i.type {
display: none;
}
:host([wed-name='info#']),
:host([wed-name='info#'])&gt;.body {
background: none;
}
:host([wed-name='warning#']){
	background-image: linear-gradient(0deg, #D51921 0%, rgba(0, 0, 0, 0) 100%);
}
:host([wed-name='warning#'])&gt;.head i.type{
	color: #D51921;	
}
:host([wed-name='warning#'])&gt;.body{
	background-color:rgba(238,28,37,0.2);
	background-image: linear-gradient(to right, #D51921 0%, rgba(0, 0, 0, 0) 100%);
}

:host([wed-name='basic#']){
	background-image: linear-gradient(0deg, #9F590E 0%, rgba(0, 0, 0, 0) 100%);
}
:host([wed-name='basic#'])&gt;.head i.type{
	color:#9F590E;
}
:host([wed-name='basic#'])&gt;.body{
	background-color:rgba(159, 89, 14, 0.2);
	background-image: linear-gradient(to right, #9F590E 0%, rgba(0, 0, 0, 0) 100%);
}
:host([wed-name='warning#'])&gt;.head i.type {
	background-position:0 4px;
}
:host([wed-name='def#'])&gt;.head i.type {
	background-position:0 -32px;
}
:host([wed-name='rem#'])&gt;.head i.type {
	background-position:0 -68px;
}
:host([wed-name='legal#'])&gt;.head i.type {
	background-position:0 -103px;
}
:host([wed-name='remind#'])&gt;.head i.type {
	background-position:0 -137px;
}
:host([wed-name='meth#'])&gt;.head i.type {
	background-position:0 -174px;
}
:host([wed-name='adv#'])&gt;.head i.type {
	background-position:0 -209px;
}
:host([wed-name='comp#'])&gt;.head i.type {
	background-position:0 -241px;
}
:host([wed-name='basic#'])&gt;.head i.type {
	background-position:0 -278px;
}
:host([wed-name='simul#'])&gt;.head i.type {
	background-position:0 -315px;
}
:host([wed-name='ex#'])&gt;.head i.type {
	background-position:0 -354px;
}
:host([wed-name='syntax#'])&gt;.head i.type {
	background-position:0 -390px;
}</sm:cssRules>
					<sm:head>
						<i class="type">
							<span>
								<sm:extractFromModelCtx>
									<sm:name/>
								</sm:extractFromModelCtx>
							</span>
						</i>
						<span class="title">
							<sm:call>
								<sm:meta/>
							</sm:call>
						</span>
					</sm:head>
					<sm:body>
						<sm:call>
							<sm:subModel/>
						</sm:call>
					</sm:body>
				</sm:headBodyContentBox>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>