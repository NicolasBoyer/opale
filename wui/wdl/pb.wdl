<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Pb/pb.model"/>
	<sm:editPoints>
		<sm:tag refCodes="info">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc"/>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="comp">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/complement.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="meth">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/method.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="remind">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/remind.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="syntax">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/syntax.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="legal">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/legal.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="simul">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/simu.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="def">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/def.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="ex">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/example.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="rem">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/remark.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="adv">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/advice.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="basic">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/basic.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="warning">
			<sm:headBodyWidget layout="float">
				<sm:container class="labelBase bloc">
					<sm:cssRules xml:space="preserve">:host([wed-name='warning#']){
    border-left: 2px solid var(--alt1-color);
    background: var(--alt1-bgcolor);
}</sm:cssRules>
					<sm:head>
						<sm:prefixIcon sc:refUri="/wui/wdl/_tags/warning.svg"/>
					</sm:head>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>