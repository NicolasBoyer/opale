<?xml version="1.0"?>
<sm:freeWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml" xmlns:wed="scenari.eu:wed">
	<sm:model sc:refUri="/academic/model/Ua/richStream/richStreamUa.model"/>
	<sm:bindsRegistry>
		<wed:bind wedlet="Box" eltName="op:richStreamUa" label="Activité média enrichi">
			<box-ctn class="v sm-root" skin="box/head-body" skinOver="box/head-body/block " wed-name="op_richStreamUa#">
				<div class="head">
					<box-label class="label">
						<sm:addAttribute name="title">
							<sm:extractFromModelCtx>
								<sm:quickHelp/>
							</sm:extractFromModelCtx>
						</sm:addAttribute>
					</box-label>
				</div>
				<div class="body">
					<wed:slot>
						<sm:refChildModel sc:refUri="/academic/model/Ua/richStream/richStreamUaM.model" editorType="hideHeader"/>
					</wed:slot>
					<wed:slot>
						<div style="color: var(--warning-color); text-align: center; font-size: 1.4em; font-style: italic; margin: 1em 0;">￼;La segmentation temporelle n'est pas encore implémentée dans l'interface web, merci d'utiliser l'application Scenari.￼</div>
					</wed:slot>
					<wed:slot>
						<wed:children select="sp:segment" defaultDisplay="sp:segment">
							<wed:bind label="Segment" eltName="sp:segment" wedlet="Box">
								<box-collaps class="v sm-sub-level" wed-name="segment#" draggable="true" skin="box/head-body" skinOver="box/head-body/block">
									<div class="head">
										<box-label class="label">
											<sm:addAttribute name="title">
												<sm:extractFromModelCtx>
													<sm:freeSelect xpath="sm:part[@code='segment']/sm:help/@quickHelp"/>
												</sm:extractFromModelCtx>
											</sm:addAttribute>
										</box-label>
									</div>
									<div class="body">
										<wed:slot>
											<wed:children select="sc:temporal">
												<wed:display eltName="sc:temporal" wedlet="BoxHidden"/>
											</wed:children>
											<sm:refChildModel sc:refUri="/academic/model/Ua/richStream/segment.model" editorType="hideHeader"/>
											<wed:children/>
										</wed:slot>
									</div>
								</box-collaps>
							</wed:bind>
						</wed:children>
						<wed:children/>
					</wed:slot>
				</div>
			</box-ctn>
		</wed:bind>
	</sm:bindsRegistry>
</sm:freeWdl>