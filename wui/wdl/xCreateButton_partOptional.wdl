<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Re/simu.model"/>
	<sm:model sc:refUri="/academic/model/Ua/richStream/segment.model"/>
	<sm:model sc:refUri="/academic/model/metaData/transcript.model"/>
	<sm:editPoints>
		<sm:tag refCodes="compl quizList acc">
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>