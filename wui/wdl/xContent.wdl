<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Ue/ue.model"/>
	<sm:model sc:refUri="/academic/model/Ue/ueDiv.model"/>
	<sm:model sc:refUri="/academic/model/Ua/courseUa.model"/>
	<sm:model sc:refUri="/academic/model/Ua/assmntUa.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/coQuiz.model"/>
	<sm:editPoints>
		<sm:tag refCodes="div courseUa assmntUa richStreamUa courseUc practUc">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561135108699">ue ueDiv courseUa =&gt; à mettre dans un wdl indépendant</comment></comment>-->
			<sm:headBodyWidget layout="float" markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:callMetaAndSubModel preferedSubModelView="externalized">
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="trainUcMcqSur trainUcMcgSur trainUcMcqMur trainUcMcgMur trainUcMatch trainUcOrdWord trainUcCloze trainUcField trainUcNumerical trainUcCoQuiz">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561135040603">ue ueDiv courseUa coQuiz =&gt; à mettre dans un wdl indépendant</comment></comment>-->
			<sm:headBodyWidget layout="float" markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:callMetaAndSubModel preferedSubModelView="externalized">
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="quiz">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561134975194" updateTime="1561136146159">assmntUa</comment></comment>-->
			<sm:headBodyWidget markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:callMetaAndSubModel preferedSubModelView="externalized">
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>