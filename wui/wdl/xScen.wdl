<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Ue/ue.model"/>
	<sm:model sc:refUri="/academic/model/Ue/ueDiv.model"/>
	<sm:model sc:refUri="/academic/model/Ua/courseUa.model"/>
	<sm:model sc:refUri="/academic/model/Ua/assmntUa.model"/>
	<sm:editPoints>
		<sm:tag refCodes="intro conclu">
			<sm:headBodyWidget layout="float">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="obj genRef">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561502346132">contnu non filtrable</comment></comment>-->
			<sm:headBodyWidget>
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="quest">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561136204477">courseUa</comment></comment>-->
			<sm:headBodyWidget layout="float">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>