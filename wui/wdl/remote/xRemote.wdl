<?xml version="1.0"?>
<sm:remoteBinaryBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/binaries/remoteDocument/remoteDocument.model"/>
	<sm:model sc:refUri="/binaries/remoteInteractive/remoteInteractive.model"/>
	<sm:editPoints>
		<sm:locationMember>
			<sm:locationBoxWidget>
				<sm:container class="labelBase"/>
				<sm:cidButton description="￼;Sélectionner une ressource sur un serveur compatible avec le protocole CID.￼">
					<sm:cidActionIdentifiers>
						<sm:identifier codes="http://schema.org/DiscoverAction"/>
					</sm:cidActionIdentifiers>
				</sm:cidButton>
			</sm:locationBoxWidget>
		</sm:locationMember>
	</sm:editPoints>
</sm:remoteBinaryBoxWdl>