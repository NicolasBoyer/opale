<?xml version="1.0"?>
<sm:remoteBinaryBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/binaries/remoteAudio/remoteAudio.model"/>
	<sm:editPoints>
		<sm:locationMember>
			<sm:locationBoxWidget>
				<sm:container class="labelBase"/>
				<sm:cidButton description="￼;Sélectionner une ressource audio sur un serveur compatible avec le protocole CID.￼">
					<sm:cidActionIdentifiers>
						<sm:identifier codes="http://schema.org/DiscoverAction"/>
					</sm:cidActionIdentifiers>
					<sm:metas>
						<sm:processing code="audio"/>
					</sm:metas>
				</sm:cidButton>
			</sm:locationBoxWidget>
		</sm:locationMember>
	</sm:editPoints>
</sm:remoteBinaryBoxWdl>