<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Re/refs/acr.model"/>
	<sm:model sc:refUri="/academic/model/Re/refs/bib.model"/>
	<sm:model sc:refUri="/academic/model/Re/refs/glos.model"/>
	<sm:model sc:refUri="/academic/model/Re/refs/ref.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:headBodyWidget>
				<sm:container appendSharedCssKey="custom-refs-tags"/>
			</sm:headBodyWidget>
		</sm:root>
	</sm:editPoints>
</sm:compositionBoxWdl>