<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Uc/practUc.model"/>
	<sm:editPoints>
		<sm:tag refCodes="desc quest">
			<sm:headBodyWidget>
				<sm:container class="bloc">
					<sm:cssRules xml:space="preserve">:host {
    border-left: 4px solid var(--alt1-bgcolor);
padding: 0 !important;
}

:host &gt; .head{
background-color: var(--alt1-bgcolor);
}

:host &gt;.head&gt;.label{
   color: var(--alt1-color);
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>