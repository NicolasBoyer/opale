<?xml version="1.0"?>
<sma:assmntUnitEditBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/cloze.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/field.model"/>
	<sm:editPoints>
		<sm:globalExplanationMember>
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitEditBoxWdl>