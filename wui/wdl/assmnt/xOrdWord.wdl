<?xml version="1.0"?>
<sma:assmntUnitMlqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/ordWord.model"/>
	<sm:editPoints>
		<sm:interactionMember>
			<sm:mlqInteractionBoxWidget>
				<sm:interactionsBox focusable="true">
					<sm:container style="display:flex; flex-direction: row; flex-wrap: wrap;"/>
				</sm:interactionsBox>
			</sm:mlqInteractionBoxWidget>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMlqBoxWdl>