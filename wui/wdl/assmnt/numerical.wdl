<?xml version="1.0"?>
<sma:assmntUnitEditBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/numerical.model"/>
	<sm:editPoints>
		<sm:interactionMember>
			<sm:editInteractionBoxWidget>
				<sm:solutionBox>
					<sm:container>
						<sm:head style="flex: 1 0 25em !important"/>
					</sm:container>
				</sm:solutionBox>
			</sm:editInteractionBoxWidget>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitEditBoxWdl>