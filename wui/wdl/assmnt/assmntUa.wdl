<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sext="http://www.utc.fr/ics/scenari/v3/modeling/extension" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" sext:assmnt="true">
	<sm:model sc:refUri="/academic/model/Ua/assmntUa.model"/>
	<sm:editPoints>
		<sm:feedbackMember>
			<sm:feedbackBoxWidget>
				<sm:feedbacksEntryBox>
					<sm:container>
						<sm:cssRules xml:space="preserve">:host &gt; .head ::slotted(*) { 
padding-left: .4rem;
margin-left: .4rem;
}</sm:cssRules>
						<sm:head style="flex-direction: column; align-items: flex-start;"/>
					</sm:container>
				</sm:feedbacksEntryBox>
			</sm:feedbackBoxWidget>
		</sm:feedbackMember>
	</sm:editPoints>
</sm:compositionBoxWdl>