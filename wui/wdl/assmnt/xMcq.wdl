<?xml version="1.0"?>
<sma:assmntUnitMcqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/mcqMur.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/mcqSur.model"/>
	<sm:editPoints>
		<sm:choicesMember>
			<sm:choicesBoxWidget>
				<sm:choiceExplanationBox>
					<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561452625434">createButton ?</comment></comment>-->
				</sm:choiceExplanationBox>
			</sm:choicesBoxWidget>
		</sm:choicesMember>
		<sm:globalExplanationMember>
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMcqBoxWdl>