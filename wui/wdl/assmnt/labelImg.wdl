<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Uc/quiz/utils/labelImg.model"/>
	<sm:editPoints>
		<sm:tag refCodes="img">
			<sm:skippedWidget/>
		</sm:tag>
		<sm:tag refCodes="desc">
			<sm:headBodyWidget collapsed="forbidden">
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>