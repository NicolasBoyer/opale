<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/metaData/resInfoM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="pubMode index">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">:host(.h) &gt; .head {
    flex: 1 2 20rem;
}
:host(.h) &gt; .body {
    flex: 4 1 10rem;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="instruct">
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>