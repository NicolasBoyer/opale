<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/root/webRootM.model"/>
	<sm:model sc:refUri="/academic/model/root/odRootM.model"/>
	<sm:model sc:refUri="/academic/model/root/presRootM.model"/>
	<sm:editPoints>
		<sm:tag refCodes="plan quest glos acr ref bib index credits solPractUc solQuiz solAssmntUa resource uc">
			<sm:headBodyWidget>
				<sm:container>
					<sm:cssRules xml:space="preserve">:host(.h) &gt; .head {
    flex: 1 1 16rem;
}
:host(.h) &gt; .body {
    flex: 8 2 20rem;
}</sm:cssRules>
				</sm:container>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="searchSettings">
			<sm:headBodyWidget layout="vertical"/>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>