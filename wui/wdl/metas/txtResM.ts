import * as dom from "lib/commons/xml/dom";
import {IBoxEnumEltWedlet} from "back/edit/wed/wedlets/box/boxTags";
import {IEndPoint} from "lib/commons/io/io";

export async function jslibAsyncInit(jsEndPoint: IEndPoint)  {
	const { DOM } = await jsEndPoint.importJs(":lib:commons/xml/dom.js") as typeof dom;

	/*
		Le fichier JS peut-être appelé plusieurs fois si il y a plusieurs éditeurs (opale et preview par exemple).
		On teste donc l'enregistrement du customElements pour ne pas le faire une seconde fois
	 */
	if (!customElements.get('op-box-txtres-pos-enum')) {
		/* La classe du customElement BoxInputEnum n'est pas exportée, on récupère donc le prototype dans le registre */
		class BoxTxtResPosEnum extends (customElements.get("box-input-enum") as Constructor<IBoxEnumEltWedlet>) {
			txtBox: HTMLElement;
			binded: boolean;

			/* Le refresh peut se produire avant la connexion de l'élément et inversement, on teste donc réciproquement l'état connecté / bindé */
			connectedCallback() {
				super.connectedCallback();
				if (this.binded) this.updateTxtRes(this.getKey());
			}

			refreshBindValue(val: string) {
				super.refreshBindValue(val);
				this.binded = true;
				if (this.isConnected) this.updateTxtRes(val);

			}

			updateTxtRes(pos: string) {
				if (!this.txtBox) {
					/* Recherche de l'élément txtRes */
					this.txtBox = DOM.findParent(this, null, (n: Node): n is HTMLElement => {
						return DOM.IS_element(n) && n.getAttribute('wed-name') == 'op_txtRes#';
					});
				}
				if (this.txtBox) {
					/* Ajout de l'attribut */
					if (pos) this.txtBox.setAttribute('imgPos', pos);
					else this.txtBox.removeAttribute('imgPos');
				}
			}
		}

		customElements.define('op-box-txtres-pos-enum', BoxTxtResPosEnum);
	}
}
