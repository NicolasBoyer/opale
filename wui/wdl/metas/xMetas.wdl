<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/metaData/uM.model"/>
	<sm:model sc:refUri="/academic/model/metaData/exeM.model"/>
	<sm:model sc:refUri="/academic/model/metaData/info.model"/>
	<sm:model sc:refUri="/academic/model/metaData/infoBin.model"/>
	<sm:editPoints>
		<sm:tag refCodes="info">
			<sm:headBodyWidget>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="desc cpyrgt">
			<sm:headBodyWidget layout="horizontal-prefered"/>
		</sm:tag>
		<sm:tag refCodes="keywds">
			<sm:headBodyWidget layout="horizontal"/>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>