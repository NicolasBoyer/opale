<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/academic/model/_edt/filters/filter.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:widgetLib sc:refUri="/wui/wdl/metas/filter.doss"/>
				<sm:content wedlet="BoxFilter">
					<sm:addAttribute name="box:entryName" namespace="scenari.eu:wed:box">sp:exclude</sm:addAttribute>
					<sm:addAttribute name="label">￼;Filtre￼</sm:addAttribute>
					<box-static class="h">
						<sm:cssRules>:host{
min-width: unset !important;
}
:host(.virtual){
	opacity: .5;
}
input{
-webkit-appearance: none;
width: 18px;
min-width: 18px;
height: 18px;
background-image: url("[WIDGETLIB]/filters.svg");
}
input:focus{
 outline: var(--edit-input-focus);
}
input[value=standard]:checked {
background-position: 0px 18px;
}
input[value=short] {
background-position: 18px 0px;
}
input[value=short]:checked {
background-position: 18px 18px;
}
input:hover:not(:disabled) {
    outline: 1px solid lightgrey; /* TODO remplacer par un filtre ? */
}</sm:cssRules>
						<input type="checkbox" value="standard" title="￼Version standard￼"/>
						<input type="checkbox" value="short" title="￼Version courte￼"/>
						<slot/>
						<!--Impossible actuellement d'insérer une balise "free" <wed:children>. A voir si nécessaire... Pour uniquement afficher les structs inconnues, l'insertion directe d'une balise html <slot> devrait suffire.
<wed:children xmlns:wed="scenari.eu:wed"/>-->
					</box-static>
				</sm:content>
			</sm:openEdtWidget>
		</sm:root>
	</sm:editPoints>
</sm:dataFormBoxWdl>