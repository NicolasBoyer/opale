<?xml version="1.0"?>
<sm:dataFormBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/academic/model/metaData/keywds.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:contentBox orientation="horizontal">
					<sm:cssRules xml:space="preserve">:host{
    flex-wrap: wrap;
}</sm:cssRules>
					<sm:call>
						<sm:members membersCodes="*" axis="#current"/>
					</sm:call>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="keywd">
			<sm:openEdtWidget>
				<sm:contentBox orientation="horizontal" draggable="true">
					<sm:cssRules xml:space="preserve">:host{
    margin: .3em 1em 0 0;
border: 1px solid var(--border-color);
padding:0;
}
:host([annot=error]) ::slotted(*), :host([annot=error]) txt-root-str {
    min-width: 50px !important;
}
:host ::slotted(*) {
border:none;
}</sm:cssRules>
					<sm:addAttribute name="sc-comment">no</sm:addAttribute>
					<span>
						<sm:addAttribute name="style">font-weight:bold; color:var(--alt1-color); margin-inline-end:.2em; padding: 0 2px; cursor: grab; background-color: var(--border-color); align-self:stretch;</sm:addAttribute>#</span>
					<sm:call>
						<sm:inputStringField textStyle="margin:.1em;"/>
					</sm:call>
				</sm:contentBox>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:dataFormBoxWdl>