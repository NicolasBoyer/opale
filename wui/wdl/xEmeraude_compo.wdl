<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/emeraude/model/activity.model"/>
	<sm:model sc:refUri="/emeraude/model/activityDiv/activityDiv.model"/>
	<sm:model sc:refUri="/emeraude/model/content/pre.model"/>
	<sm:editPoints>
		<sm:tag refCodes="intro conclu">
			<sm:headBodyWidget layout="float">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="obj pre genRef">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561502346132">contnu non filtrable</comment></comment>-->
			<sm:headBodyWidget>
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="expStep practQuestStep mcgSQuestStep mcqMQuestStep mcqMQuestStep mcgMQuestStep matchQuestStep ordWQuestStep clozeQuestStep numericalQuestStep fieldQuestStep coQuizStep">
			<sm:headBodyWidget layout="float" markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:callMetaAndSubModel preferedSubModelView="externalized">
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="comment assmntUa">
			<sm:headBodyWidget layout="float" markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:callMetaAndSubModel preferedSubModelView="externalized">
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
		<sm:tag refCodes="div">
			<!--<comment xmlns="scenari.eu:comment:1.0" type="thread"><comment creationTime="1561503917758">toujours internalisé (pas d'item)</comment></comment>-->
			<sm:headBodyWidget layout="float" markLevels="true">
				<sm:container class="firstLevelLabelBase bloc"/>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>