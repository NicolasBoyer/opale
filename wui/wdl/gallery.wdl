<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/academic/model/Re/gallery.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:contentBox focusable="false">
					<sm:call>
						<sm:meta/>
					</sm:call>
					<box-static style="display: flex; flex-direction: row; flex-wrap: wrap; margin-top: 1rem;">
						<sm:call>
							<sm:parts partsCodes="img" axisParts="#current"/>
						</sm:call>
					</box-static>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="img">
			<sm:headBodyWidget>
				<sm:container style="margin-right: 30px">
					<sm:cssRules xml:space="preserve">:host ::slotted(box-ptritem[annot=error]) { 
padding: 50px;
}</sm:cssRules>
					<sm:head style="display: none;"/>
				</sm:container>
				<sm:callMetaAndSubModel>
					<sm:refItemViewing/>
				</sm:callMetaAndSubModel>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>