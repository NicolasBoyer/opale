<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Uc/quiz/coQuiz.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget>
				<sm:hideChildren/>
			</sm:skippedWidget>
		</sm:root>
		<sm:tag refCodes="quest">
			<sm:headBodyWidget>
				<sm:container class="firstLevelLabelBase bloc"/>
				<sm:ifAbsent>
					<sm:createButton/>
				</sm:ifAbsent>
			</sm:headBodyWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>