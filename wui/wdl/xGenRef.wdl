<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns="http://www.w3.org/1999/xhtml">
	<sm:model sc:refUri="/academic/model/Re/refs/genRef.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:openEdtWidget>
				<sm:contentBox orientation="horizontal">
					<sm:cssRules xml:space="preserve">:host{
    flex-wrap: wrap;
}</sm:cssRules>
					<sm:call>
						<sm:parts partsCodes="*" axisParts="#current"/>
					</sm:call>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:root>
		<sm:tag refCodes="ref">
			<sm:openEdtWidget>
				<sm:contentBox orientation="horizontal" draggable="true">
					<sm:cssRules xml:space="preserve">:host{
    margin: 10px 30px 10px 0;
border: 1px solid var(--border-color);
padding:0;
}</sm:cssRules>
					<span>
						<sm:addAttribute name="style">font-weight:bold; color:var(--alt1-color); margin-inline-end:.2em; width: 0.7em; cursor: grab; background-color: var(--border-color); align-self:stretch;</sm:addAttribute>
					</span>
					<sm:call>
						<sm:subModel/>
					</sm:call>
				</sm:contentBox>
			</sm:openEdtWidget>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>