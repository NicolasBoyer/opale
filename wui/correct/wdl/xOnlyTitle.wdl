<?xml version="1.0"?>
<sm:compositionBoxWdl xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
	<sm:model sc:refUri="/academic/model/Ua/courseUa.model"/>
	<sm:model sc:refUri="/academic/model/Ua/assmntUa.model"/>
	<sm:model sc:refUri="/academic/model/Uc/expUc.model"/>
	<sm:model sc:refUri="/academic/model/Uc/expUcDiv.model"/>
	<sm:model sc:refUri="/academic/model/Uc/practUc.model"/>
	<sm:editPoints>
		<sm:tag refCodes="*">
			<sm:hiddenWidget/>
		</sm:tag>
	</sm:editPoints>
</sm:compositionBoxWdl>