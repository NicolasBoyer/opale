<?xml version="1.0"?>
<sma:assmntUnitMlqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/ordWord.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/match.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:interactionMember>
			<sm:hiddenWidget/>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:hiddenWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMlqBoxWdl>