<?xml version="1.0"?>
<sma:assmntUnitMcqBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/mcqMur.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/mcqSur.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:choicesMember>
			<sm:hiddenWidget/>
		</sm:choicesMember>
		<sm:globalExplanationMember>
			<sm:hiddenWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitMcqBoxWdl>