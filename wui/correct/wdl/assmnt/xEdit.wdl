<?xml version="1.0"?>
<sma:assmntUnitEditBoxWdl xmlns:sma="kelis.fr:scenari/modeling/assessment" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:sm="http://www.utc.fr/ics/scenari/v3/modeling">
	<sm:model sc:refUri="/academic/model/Uc/quiz/cloze.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/field.model"/>
	<sm:model sc:refUri="/academic/model/Uc/quiz/numerical.model"/>
	<sm:editPoints>
		<sm:root>
			<sm:skippedWidget/>
		</sm:root>
		<sm:interactionMember>
			<sm:hiddenWidget/>
		</sm:interactionMember>
		<sm:globalExplanationMember>
			<sm:hiddenWidget/>
		</sm:globalExplanationMember>
	</sm:editPoints>
</sma:assmntUnitEditBoxWdl>