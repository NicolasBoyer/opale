//Fonction appelé par le contexte applicatif parent pour activer l'ihm d'activation des éditeurs

var editEntryPointHandlers = {};
if (scDynGenLib.addWuiApi()) {
	function initParentFrame(scParentFrame) {
		window.scParentFrame = scParentFrame;
	}

	function enableEditors(conf) {
		var editAnchors = document.body.querySelectorAll(conf.rootsSelector);

		// Map des ancres par srcRef
		var anchorsBySrcRef = {};
		for (var i = 0; i < editAnchors.length; i++) {
			var editAnchor = editAnchors.item(i);
			editAnchor.editorConfig = conf;

			var srcRef = getSrcRefFrom(editAnchor);
			anchors = anchorsBySrcRef[srcRef];
			if (!anchors) anchors = anchorsBySrcRef[srcRef] = [];
			anchors.push(editAnchor);
		}

		// Vérification des droits pour chaque srcRef
		Object.keys(anchorsBySrcRef).forEach(function(srcRef) {
			scParentFrame.canEdit(srcRef).then(function (editable) {
				if (!editable) return;
				// Création des boutons pour chaque ancre du srcRef
				for (var i = 0; i < anchorsBySrcRef[srcRef].length; i++) {
					var editAnchor = anchorsBySrcRef[srcRef][i];
					editAnchor.classList.add("scEditable");
					var btn = editAnchor.inlineEditorBtn = editAnchor.insertBefore(document.createElement("button"), editAnchor.children[0]);
					const label = "￼Éditer￼";
					btn.title = label;
					btn.innerHTML = "<span>" + label + "</span>";
					btn.classList.add("wui-edit");
					btn.onclick = function onEdit() {
						const cmtAnchor = scPaLib.findNode("can:.scCmtAnchor", this);
						window.scParentFrame.editFragment(cmtAnchor);
					};
				}
			}).catch(function(e) {
				console.log("enableEditors failed:", e);
			});
		});

		window.scItemLocksLstn.push((pSrcRef, pBool) => {
			var editAnchors = document.body.querySelectorAll(conf.rootsSelector);
			for (var anchor of editAnchors) {
				if (getSrcRefFrom(anchor) === pSrcRef) {
					var btn = anchor.querySelector(":scope > .wui-edit");
					if (btn) btn.disabled = pBool;
				}
			}
		});
	}

	function markAsEditing(origin) {
		//TODO
		//console.log("markAsEditing", origin);
	}

	// XXX A mutualiser dans une lib dyngen de Modeling ? Fct issue du début de lib TS: Wui_Wsp/lib/wsp/dyngen.ts
	function getSrcRefFrom(elt) {
		do {
			var ori = elt.getAttribute("data-origin");
			if (ori) {
				if (ori.charAt(0) === '@') {
					var sep = ori.indexOf('?', 1);
					return ori.substring(1, sep);
				}
			}
		} while ((elt = elt.parentElement));
		return null;
	}

	editEntryPointHandlers.ascendant = function (level) {
		this.level = level;
	};
	editEntryPointHandlers.ascendant.prototype.handleOrigin = function (origin) {
		var oriPath = origin.oriPath.split("/");
		oriPath.splice(oriPath.length - this.level);
		origin.oriPath = oriPath.join("/");
		return origin;
	};

	// Reset des libs SC
	(function () {
		window.beforeReloadPageLstn.push(function (srcRef) {
			scDynUiMgr.subWindow.fSubWins = [];
		});
		window.afterReloadPageLstn.push(function (srcRef, updates) {
			scTooltipMgr.registerTooltips();
		});
	}());

	scCmtMgr.strings.turnOffLabel = "￼Désactiver le mode correction et annotation￼";
	scCmtMgr.strings.turnOnLabel = "￼Activer le mode correction et annotation￼";
	scCmtViewWdg.prototype.updateAnchorControls = function (srcRef) {

		var addBtn = function (node) {
			var child = node.children[0];
			if (child && child.classList.contains("wui-edit")) child = child.nextElementSibling;
			if (child && child.classList.contains("insertCmt")) return;
			else {
				const before = node.children.length && node.children[0].classList.contains("wui-edit") ? node.children[1] : node.children[0];
				var btn = node.inlineEditorBtn = node.insertBefore(document.createElement("button"), before);
				const label = "￼Commenter￼";
				btn.title = label;
				btn.innerHTML = "<span>" + label + "</span>";
				btn.classList.add("insertCmt");
				btn.onclick = function onEdit(event) {
					const anchorNode = scPaLib.findNode("can:.scCommentable", this);
					if (anchorNode) {
						var anchor = scOrgn.getOriginFrom(anchorNode);
						anchor.node = anchorNode;
						scCmtMgr.create(anchor);
					}
				};
			}
		}


		scCmtMgr.anchors.forEach(function (anchor) {
			if (srcRef && anchor.srcRef !== srcRef) return;
			if (!scCmtMgr.perms || !scCmtMgr.perms[anchor.srcRef]) return;//init process
			if (scCmtMgr.perms && scCmtMgr.perms[anchor.srcRef]["Create"] && !scCmtMgr.locks[anchor.srcRef] && scCmtMgr.commentMode) {
				addBtn(anchor.node);
				anchor.node.classList.add("scCommentable");
				anchor.node.addEventListener("mouseover", this.mouseOver);
				anchor.node.addEventListener("mouseout", this.mouseOut);
			} else {
				anchor.node.classList.remove("scCommentable");
				anchor.node.removeEventListener("mouseover", this.mouseOver);
				anchor.node.removeEventListener("mouseout", this.mouseOut);
			}
		}, scCmtMgr.view);
		scCmtMgr.view.resetHover();
		scCmtMgr.listeners.fire("onAllThreadsBuilt", scCmtMgr.rootNode);
		if ("scSiLib" in window) scSiLib.fireResizedNode(scCmtMgr.rootNode);
	};
}
