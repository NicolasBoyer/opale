"use strict";

var galleryMgr = {
	fStrings : [
		"￼Jouer l'animation￼","￼Mettre en pause￼"
	],
	fIndex : 0,

	onLoad : function() {
		this.resetImages();
		this.fRoot = document.querySelector(".gallery_slider");
		this.fRoot.style.display = "";
		this.fPlayBtn = this.addButton(this.fRoot, this.fStrings[0], "animBtn playBtn");
		this.fPlayBtn.addEventListener("click", this.play);
		this.fPauseBtn = this.addButton(this.fRoot, this.fStrings[1], "animBtn pauseBtn");
		this.fPauseBtn.addEventListener("click", this.pause);
		this.fPauseBtn.style.display = "none";
	},

	resetImages : function() {
		var vImages = document.querySelectorAll(".gallery_slider>img")
		for (var i = 0; i < vImages.length; i++) {
			vImages[i].style.display = "none"; 
		}
		this.fIndex += 1;
		if (this.fIndex > vImages.length) this.fIndex = 1;
		vImages[this.fIndex-1].style.display = "";
		vImages[this.fIndex-1].style.opacity = 1;
	},

	play: function() {
		galleryMgr.fRoot.classList.add("fade");
		galleryMgr.fPauseBtn.style.display = "";
		galleryMgr.fPlayBtn.style.display = "none";
		galleryMgr.resetImages();
		galleryMgr.fTimer = setTimeout(galleryMgr.play, 2000);
	},

	pause: function() {
		galleryMgr.fRoot.classList.remove("fade");
		galleryMgr.fPauseBtn.style.display = "none";
		galleryMgr.fPlayBtn.style.display = "";
		clearTimeout(galleryMgr.fTimer);
	},

	addButton: function(pParent, pLabel, pClass) {
		var vButton = document. createElement("button");
		vButton.className = pClass;
		var vButtonSpan = document. createElement("span");
		vButtonSpan.innerHTML = pLabel
		vButton.appendChild(vButtonSpan);
		pParent.appendChild(vButton);
		return vButton;
	}
};

scOnLoads.push(galleryMgr);