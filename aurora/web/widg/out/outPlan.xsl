<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:redirect="com.scenari.xsldom.xalan.lib.Redirect" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" extension-element-prefixes="redirect" exclude-result-prefixes="sc xhtml" version="1.0">
	<xsl:output omit-xml-declaration="yes" indent="no" method="xml"/>
	<xsl:param name="vDialog"/>
	<xsl:param name="vAgent"/>
	<xsl:template match="treeContent">
		<nav role="navigation">
			<ul class="plan">
				<xsl:apply-templates select="entry[2]/*"/>
			</ul>
		</nav>
	</xsl:template>
	<xsl:template match="entry">
		<xsl:variable name="vOutlineClasses" select="computeStrDialog(concat(@dialog, '/outlineClasses'))"/>
		<xsl:variable name="vUrl" select="computeStrDialog(string(@dialog), 'act:')"/>
		<xsl:variable name="vDepth" select="count(ancestor::entry)"/>
		<xsl:if test="$vOutlineClasses != 'hidden'">
			<li class="type_{if(entry,'b','l')} dpt_{$vDepth} {$vOutlineClasses}">
				<div class="lbl type_{if(entry,'b','l')}">
					<a href="{$vUrl}" target="_self" class="item">
						<span>
							<xsl:value-of select="@title"/>
						</span>
					</a>
				</div>
				<xsl:if test="entry">
					<ul class="sub">
						<xsl:apply-templates/>
					</ul>
				</xsl:if>
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template match="node()"/>
</xsl:stylesheet>