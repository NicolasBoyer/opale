Attributs possibles sur le contrôleur :
 - data-te-timelines : permet de sélectionner les timelines, '.teTimeline' par défaut
 - data-te-container : optionel, permet d'isoler la recherche du media et des segments à une racine, document.body par défaut
 - data-te-media : optionel, permet de sélectionner le media à controler, '.teMedia' par défaut
 - data-te-content-area : optionel, permet de sélectionner la zone de contenu, pour le stylage (ajout des classes teActiveSegment ou teActivePoint) et pour filter la gestion du hash, '.teContentArea' par défaut
 - data-te-segments : optionel, permet de sélectionner les segments à temporiser, '[data-te-start],[data-te-segment-target]' par défaut
 - data-te-points : optionel, permet de sélectionner les pauses, '[data-te-position],[data-te-point-target]' par défaut
Les éléments dont la classe commence par 'te' sont géré par le controleur et sont tous optionnels
