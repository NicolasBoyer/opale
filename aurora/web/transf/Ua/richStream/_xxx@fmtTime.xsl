<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" xmlns:op="utc.fr:ics/opale3"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="sc sp op">

    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />

    <xsl:param name="vDialog" />
    <xsl:param name="vAgent" />

    <xsl:template name="tFormatTime">
        <xsl:param name="pDuration" />

        <xsl:variable name="vHours" select="floor($pDuration div 3600)" />
        <xsl:variable name="vMinutes" select="floor($pDuration mod 3600 div 60)" />
        <xsl:variable name="vSeconds" select="round($pDuration mod 60)" />

        <xsl:if test="$vHours">
            <xsl:value-of select="$vHours" />
            <xsl:text>:</xsl:text>
        </xsl:if>
        <xsl:if test="$vHours and $vMinutes &lt; 10">0</xsl:if>
        <xsl:value-of select="$vMinutes" />
        <xsl:text>:</xsl:text>
        <xsl:if test="$vSeconds &lt; 10">0</xsl:if>
        <xsl:value-of select="$vSeconds" />

    </xsl:template>

    <xsl:template match="op:segment">
        <xsl:variable name="vTeNode" select="(ancestor-or-self::sp:*[sc:temporal])[last()]" />

        <xsl:variable name="vStart" select="$vTeNode/sc:temporal/sc:start"/>

        <xsl:call-template name="tFormatTime">
            <xsl:with-param name="pDuration" select="$vStart"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="op:coQuiz">
		 <xsl:variable name="vTeNode" select="(ancestor-or-self::sp:*[sc:temporal])[last()]" />

		 <xsl:variable name="vEnd" select="$vTeNode/sc:temporal/sc:end"/>

		 <xsl:call-template name="tFormatTime">
			 <xsl:with-param name="pDuration" select="$vEnd"></xsl:with-param>
		 </xsl:call-template>
	 </xsl:template>
</xsl:stylesheet>
