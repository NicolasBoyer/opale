<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2"
         xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
       exclude-result-prefixes="xsi adlcp"
       version="1.0">
    <xsl:output encoding="UTF-8" method="xml"/>
    <xsl:param name="vAgent" />
    <xsl:param name="vDialog" />
    
    <xsl:template match="/*">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xmlns:xsi">http://www.w3.org/2001/XMLSchema-instance</xsl:attribute>
            <xsl:attribute name="xmlns:adlcp">http://www.adlnet.org/xsd/adlcp_rootv1p2</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*[name()='item']" priority="100">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <adlcp:dataFromLMS>'passingScore':<xsl:value-of select="genProp('passingScore@stored') div 100"/></adlcp:dataFromLMS>
        </xsl:copy>
    </xsl:template>
    
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>