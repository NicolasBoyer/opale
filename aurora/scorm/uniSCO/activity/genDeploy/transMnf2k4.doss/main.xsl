<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
       xmlns="http://www.imsglobal.org/xsd/imscp_v1p1"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:adlseq="http://www.adlnet.org/xsd/adlseq_v1p3"
       xmlns:imsss="http://www.imsglobal.org/xsd/imsss"
       xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_v1p3"
       xmlns:adlnav="http://www.adlnet.org/xsd/adlnav_v1p3"
       xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
       exclude-result-prefixes="xsi adlseq imsss adlcp adlnav"
       version="1.0">
    <xsl:output encoding="UTF-8" method="xml"/>
    <xsl:param name="vAgent" />
    <xsl:param name="vDialog" />
    
    <xsl:template match="/*">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:attribute name="xmlns:xsi">http://www.w3.org/2001/XMLSchema-instance</xsl:attribute>
            <xsl:attribute name="xmlns:adlseq">http://www.adlnet.org/xsd/adlseq_v1p3</xsl:attribute>
            <xsl:attribute name="xmlns:imsss">http://www.imsglobal.org/xsd/imsss</xsl:attribute>
            <xsl:attribute name="xmlns:adlcp">http://www.adlnet.org/xsd/adlcp_v1p3</xsl:attribute>
            <xsl:attribute name="xmlns:adlnav">http://www.adlnet.org/xsd/adlnav_v1p3</xsl:attribute>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*[name()='item']" priority="100">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
             <imsss:sequencing>
                <imsss:objectives>
                    <imsss:primaryObjective satisfiedByMeasure="true">
                        <imsss:minNormalizedMeasure><xsl:value-of select="genProp('passingScore@stored') div 100"/></imsss:minNormalizedMeasure>
                    </imsss:primaryObjective>
                </imsss:objectives>
            </imsss:sequencing>
        </xsl:copy>
    </xsl:template>
    
    
    <item identifier="un0011" isvisible="true" identifierref="un0011_RES">
   <title>Avaliação de Módulo</title>
   <imsss:sequencing>
    <imsss:objectives>
     <imsss:primaryObjective objectiveID="PRIMARYOBJ" satisfiedByMeasure="true">
      <imsss:minNormalizedMeasure>0.6</imsss:minNormalizedMeasure>
     </imsss:primaryObjective>
    </imsss:objectives>
    <imsss:deliveryControls completionSetByContent="true" objectiveSetByContent="true"/>
   </imsss:sequencing>
  </item>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>