<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
		xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
		xmlns:op="utc.fr:ics/opale3"
		xmlns:emrd="scpf.org:opale/emeraude"
		xmlns:xalan="http://xml.apache.org/xalan"
		xmlns:sfm="http://www.utc.fr/ics/scenari/v3/filemeta"
		exclude-result-prefixes="sc sp xalan op">
	<xsl:output encoding="UTF-8" method="xml"/>
	
	<xsl:param name="pCurrentItem"/>
	<xsl:param name="pCurrentItemUri"/>

	<xsl:template match="emrd:webRoot">
		<op:webRootA>
			<xsl:apply-templates select="@*|node()"/>
		</op:webRootA>
	</xsl:template>

	<xsl:template match="emrd:webRootM">
		<op:webRootAM>
			<xsl:apply-templates select="@*|node()"/>
		</op:webRootAM>
	</xsl:template>
	
	<xsl:template match="emrd:odRoot">
		<op:odRootA>
			<xsl:apply-templates select="@*|node()"/>
		</op:odRootA>
	</xsl:template>

	<xsl:template match="emrd:odRootM">
		<op:odRootAM>
			<xsl:apply-templates select="@*|node()"/>
		</op:odRootAM>
	</xsl:template>
	
	<xsl:template match="emrd:webRootM/sp:auth/op:txt">
		<op:sTxt>
			<xsl:apply-templates select="@*|node()"/>
		</op:sTxt>
	</xsl:template>

	<xsl:template match="emrd:webRootM/sp:contrib">
		<xsl:comment>Une structure Contributeurs non reconnue à partir de Opale 3.6 à été supprimée. Ci-dessous les contenus textuels résiduels:
			<xsl:copy>
				<xsl:apply-templates select="@*|node()"/>
			</xsl:copy>
		</xsl:comment>
	</xsl:template>

	<xsl:template match="emrd:webRootM/sp:logos">
		<xsl:comment>Une structure Logos non reconnue à partir de Opale 3.6 à été supprimée. Ci-dessous les contenus textuels résiduels:
			<xsl:copy>
				<xsl:apply-templates select="@*|node()"/>
			</xsl:copy>
		</xsl:comment>
		<xsl:if test="count(sp:logo)&gt;1">
			<xsl:comment>Ce module Emeraude incluait <xsl:value-of select="count(sp:logo)"/> logos. Seul le premier de la liste a été gardé.</xsl:comment>
		</xsl:if>
		<sp:logo sc:refUri="{sp:logo[1]/sp:image/@sc:refUri}"/>
	</xsl:template>

	<xsl:template match="emrd:webRootM/sp:settings/sp:results">
		<xsl:if test="text()='yes'">
			<xsl:comment>Ce module Émeraude était paramétré pour inclure une page de résultats.</xsl:comment>
			<xsl:comment>Le paramètre "Publier la page de résultats" n'existe pas dans Opale 3.6. Émeraude permet maintenant de proposer une activité d'auto-évaluation en fin de module. Dans un contexte SCORM le résultat de cette évaluation est alors remonté à la platforme.</xsl:comment>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
	 	</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
