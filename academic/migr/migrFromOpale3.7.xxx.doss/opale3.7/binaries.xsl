<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" 
		xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"
		xmlns:sfm="http://www.utc.fr/ics/scenari/v3/filemeta"
		xmlns:xalan="http://xml.apache.org/xalan"
		exclude-result-prefixes="sc sp sfm xalan">
	<xsl:output encoding="UTF-8" method="xml"/>
	
	<xsl:param name="pCurrentItem"/>
	<xsl:param name="pCurrentItemUri"/>

	<!-- IMAGE -->
	<xsl:template match="sfm:png">
		<sfm:png_gif_jpg_jpeg>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:png_gif_jpg_jpeg>
	</xsl:template>
	<xsl:template match="sfm:gif">
		<sfm:png_gif_jpg_jpeg>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:png_gif_jpg_jpeg>
	</xsl:template>
	<xsl:template match="sfm:jpg_jpeg">
		<sfm:png_gif_jpg_jpeg>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:png_gif_jpg_jpeg>
	</xsl:template>

	<!-- AUDIO -->
	<xsl:template match="sfm:mp3">
		<sfm:mp3_oga_ogg>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:mp3_oga_ogg>
	</xsl:template>
	<xsl:template match="sfm:oga_ogg">
		<sfm:mp3_oga_ogg>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:mp3_oga_ogg>
	</xsl:template>

	<!-- video -->
	<xsl:template match="sfm:mp4_f4v">
		<sfm:mp4_f4v_webm>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:mp4_f4v_webm>
	</xsl:template>
	<xsl:template match="sfm:webm">
		<sfm:mp4_f4v_webm>
			<xsl:apply-templates select="@*|node()"/>
	 	</sfm:mp4_f4v_webm>
	</xsl:template>
	
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
	 	</xsl:copy>
	</xsl:template>
		
</xsl:stylesheet>
