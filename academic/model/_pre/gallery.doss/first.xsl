<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:xul="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul" 
		xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive" 
		xmlns:sc="http://www.utc.fr/ics/scenari/v3/core" version="1.0">

	<xsl:output method="xml" indent="no"/>

	<!-- L'aperçu le la première image est converti en JPEG sur fond blanc avec suppression de l'alpha puis réduits à 360 x 160 -->
	<xsl:template match="*">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="sp:img[1]"><xsl:if test="@sc:refUri"><xul:itemimage flex="1" refUri="{@sc:refUri}" scaleproportionnal="true" transform="transform=imageMagick&amp;command=convert%20%24src%20-background%20%22#FFFFFF%22%20-flatten%20-alpha%20off%20-quality%2070%20%24dst&amp;amp;outType=JPEG&amp;transform=img2img&amp;outType=JPEG&amp;sizeRules=Px(ScSCS(fontPt'10')Bounds(maxW'360'maxH'160'))"/></xsl:if></xsl:template>
	<xsl:template match="sp:img[2]"><xsl:if test="@sc:refUri"><xul:spacer flex="1"/></xsl:if></xsl:template>
	<xsl:template match="text()"/>
</xsl:stylesheet>

