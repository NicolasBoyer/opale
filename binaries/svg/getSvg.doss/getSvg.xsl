<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:cc="http://creativecommons.org/ns#"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns="http://www.w3.org/2000/svg"	version="1.0"
	exclude-result-prefixes="">

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes" encoding="UTF-8"/>

	<xsl:param name="vComp"/>

	<xsl:template match="/svg">
		<svg>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="node()"/>
		</svg>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
